#pragma once
#include <iostream>
#include <cstdio>
#include <cmath>
#include <queue>
#include "nodoB.hpp"
using namespace std;

template<class clave>
class arbolB{
private:
	nodoB<clave>* raiz_;
	int nNodos;

	//Modo estadistica
	int nCompB;	//Numero de comparaciones en la busqueda
	int nCompI;	//Numero de comparaciones en la inserción

public:

	arbolB():
		raiz_(NULL),
		nNodos(0),
		nCompB(0),
		nCompI(0){}

	~arbolB(){
		BorrarArbol(raiz_);
	}

	void BorrarArbol(nodoB<clave>* raiz){
		if(raiz -> get_i() != NULL )
			BorrarArbol(raiz -> get_i());
		if(raiz -> get_d() != NULL )
			BorrarArbol(raiz -> get_d());
		delete raiz;
	}

	void inserta(const clave& dato){
		insertaR(dato, raiz_);
		nNodos++;
	}

	void insertaR(const clave& dato, nodoB<clave>* &nodo){
		nCompI++;
		if(nodo == NULL)
			nodo = new nodoB<clave>(dato);
		else if(dato < nodo->get_dato())
			insertaR(dato, nodo->get_i());
		else
			insertaR(dato, nodo->get_d());
	}

	nodoB<clave>* busca(const clave &dato){
		return buscaR(dato, raiz_);
	}

	nodoB<clave>* buscaR(const clave &dato, nodoB<clave>* &raiz){
		nCompB++;
		if(raiz == NULL)
			return NULL;
		if(raiz -> get_dato() == dato)	
			return raiz;
		if(dato < raiz -> get_dato())
			return buscaR(dato, raiz -> get_i());
		return buscaR(dato, raiz -> get_d());
	}

	void elimina(const clave &dato){
		eliminaR(dato, raiz_);
		nNodos--;
	}

	void eliminaR(const clave &dato, nodoB<clave>* &raiz){
		if(raiz == NULL) return;
		if(dato < raiz -> get_dato())
			eliminaR(dato, raiz->get_i());
		else if(dato > raiz -> get_dato())
			eliminaR(dato, raiz->get_d());
		else{
			nodoB<clave>* elm = raiz;
			if(raiz -> get_d() == NULL)
				raiz = raiz -> get_i();
			else if(raiz -> get_i() == NULL)
				raiz = raiz -> get_d();
			else
				sustituye(elm, raiz->get_i());

			delete elm;
		}
	}

	void sustituye(nodoB<clave>* &elm, nodoB<clave>* &sust){
		if( sust->get_d() != NULL)
			sustituye(elm, sust->get_d());
		else{
			elm -> set_dato(sust -> get_dato());
			elm = sust;
			sust = sust -> get_i();
		}
	}

	void print(){
		printR(raiz_);
	}
	void printR(nodoB<clave>* &nodo){
		if(nodo == NULL) return;
		printR(nodo->get_i());
		cout << nodo->get_dato();
		printR(nodo->get_d());
	}

	//Metodos getter y setter para los numeros de comparaciones
	inline int get_nCompB() const{
		return nCompB;
	}

	inline int get_nCompI() const{
		return nCompI;
	}

	inline void set_nCompB(const int& n){
		nCompB = n;
	}

	inline void set_nCompI(const int& n){
		nCompI = n;
	}

	void recorreN(){
		queue<nodoB<clave>*> Qd;	//Cola nodos
		queue<int> Qn;		//Cola niveles
		nodoB<clave>* nodo;
		int nivel, Nivel_actual=-1;

		Qd.push(raiz_);	Qn.push(0);

		while(!Qd.empty()){
			nodo = Qd.front();
			Qd.pop();
			nivel = Qn.front();
			Qn.pop();

			if(nivel > Nivel_actual){
				Nivel_actual = nivel;
				cout << endl << "L" << Nivel_actual << ":";
			}
			if(nodo != NULL){
				cout << "[" << nodo -> get_dato() << "]";

				Qd.push(nodo->get_i());	Qn.push(nivel+1);
				Qd.push(nodo->get_d());	Qn.push(nivel+1);
			}
			else
				cout << "[  NULL  ]";
		}

		cout << endl;
	}

	inline void Procesar(nodoB<clave>* nodo){
		cout << "[" << nodo -> get_dato() << "] ";
	}


};