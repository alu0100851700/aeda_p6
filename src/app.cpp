#include "arbolB.hpp"
#include <cstdio>
#include "dni.hpp"

void clear(){
	system("clear");
	cout << "--- Practica 6: Arbol de Busqueda Binario ---" << endl;
	cout << "---          David Redondo Durand         ---\n" << endl;
}

void demostracion();
void estadistica();

int main(){
	clear();

	bool error_sel;

	do{
		error_sel = false;
		cout << "Seleccione el modo:\n"
		<< "1. Modo demostracion\n"
		<< "2. Modo estadistica"
		<< endl;
		int modo;
		cin >> modo;

		switch (modo){
			case 1:
				clear();
				demostracion();
				break;
			case 2:
				clear();
				estadistica();
				break;
			default:
				error_sel = true;
				clear();
				cerr << "Fallo en la seleccion!"<< endl;
				break;
		}
	}while(error_sel);
	

	return 0;
}


void demostracion(){
	cout << "Arbol vacio" << endl;

	arbolB<dni>* arbol;
	arbol = new arbolB<dni>;
	


	bool error_sel;
	int sel;
	do{
		error_sel = false;

		arbol -> recorreN();

		cout << "\n0. Salir\n" 
		<< "1. Insertar\n"
		<< "2. Eliminar" << endl;
		cin >> sel;

		switch (sel){
			int n_dni;
			case 0:
				break;
			case 1:
				arbol -> inserta(30000000 + rand()%50000000);
				break;
			case 2:
				cout << "Nodo a eliminar: ";
				cin >> n_dni;
				arbol -> elimina(n_dni);
				break;
			default:
				error_sel = true;
				clear();
				cerr << "Fallo en la seleccion!"<< endl;
				break;
		}
	}while(error_sel || sel!=0);
	system("clear");
	cout << "Programa finalizado" << endl;

}

void estadistica(){
	int nNodos=0;

	cout << "Numero de nodos: ";
	cin >> nNodos;

	int nPruebas=0;
	
	cout << "Numero de pruebas: ";
	cin >> nPruebas;

	//Creación de los dnis
	dni* a_dni = new dni[nNodos+nPruebas];

	//Creación del arbol binario de busqueda
	arbolB<dni>* arbol;
	arbol = new arbolB<dni>;

	int maxI = 0;
	int minI = 0;
	int medI = 0;

	cout << "\t\tN" << "\tP" << "\tMinimo" << "\tMedio" << "\tMaximo" << endl;
	///////////////////////////////////////////////////////////////////
	// Insercion: Agregar los dnis al arbol
	///////////////////////////////////////////////////////////////////
	for(int i=0; i<nNodos; i++){
		arbol -> set_nCompI(0);
		arbol -> inserta(a_dni[i]);
		medI += arbol -> get_nCompI();
		if(minI > arbol -> get_nCompI() || minI == 0)	minI = arbol -> get_nCompI();
		if(maxI < arbol -> get_nCompI() || maxI == 0)	maxI = arbol -> get_nCompI();
	}
	medI /= nNodos;
	cout << "Insercion\t" << nNodos << "\t" << nPruebas << "\t" << minI << "\t" << medI << "\t" << maxI << endl;



	///////////////////////////////////////////////////////////////////
	// Busqueda 1: Valores que han sido introducidos anteriormente
	///////////////////////////////////////////////////////////////////
	int maxB = 0;
	int minB = 0;
	int medB = 0;

	int nN;	//Numero aleatorio entre 0 y nNodos
	//Busqueda de valores dentro de N
	for(int i=0; i<nPruebas; i++){
		arbol -> set_nCompB(0);
		nN = rand()%nNodos;

		arbol -> busca(a_dni[nN]);

		medB += arbol -> get_nCompB();
		if(minB > arbol -> get_nCompB() || minB == 0)	minB = arbol -> get_nCompB();
		if(maxB < arbol -> get_nCompB() || maxB == 0)	maxB = arbol -> get_nCompB();
	}
	medB /= nPruebas;

	cout << "Busqueda1\t" << nNodos << "\t" << nPruebas << "\t" << minB << "\t" << medB << "\t" << maxB << endl;



	///////////////////////////////////////////////////////////////////
	// Busqueda 2: Valores que NO han sido introducidos anteriormente
	///////////////////////////////////////////////////////////////////
	maxB = 0;
	minB = 0;
	medB = 0;

	int nP;	//Numero aleatorio entre nNodos y nPruebas, que son las claves que no se introdujeron
	for(int i=0; i<nPruebas; i++){
		arbol -> set_nCompB(0);
		nP = nNodos + rand()%nPruebas;

		arbol -> busca(a_dni[nP]);

		medB += arbol -> get_nCompB();
		if(minB > arbol -> get_nCompB() || minB == 0)	minB = arbol -> get_nCompB();
		if(maxB < arbol -> get_nCompB() || maxB == 0)	maxB = arbol -> get_nCompB();
	}

	medB /= nPruebas;

	cout << "Busqueda2\t" << nNodos << "\t" << nPruebas << "\t" << minB << "\t" << medB << "\t" << maxB << endl;

	delete[] a_dni;
}