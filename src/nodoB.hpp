#pragma once
#include <cstdio>
using namespace std;

template <class clave>
class nodoB{
private:
	clave dato_;
	nodoB<clave>*	i_;	
	nodoB<clave>*	d_;	

public:
	nodoB(const clave& dato):
		dato_(dato),
		i_(NULL),
		d_(NULL){}

	nodoB(void){
		dato_ = new clave();
		i_ = NULL;
		d_ = NULL;
	}

	~nodoB(void){}

	//GET METHODS
	clave get_dato(void) const{
		return dato_;
	}

	nodoB<clave>*& get_i(void){
		return i_;
	}

	nodoB<clave>*& get_d(void){
		return d_;
	}

	//SET METHODS
	void set_dato(const clave &dato){
		dato_ = dato;
	}

	void set_i(const nodoB<clave>* i){
		i_ = i;
	}

	void set_d(const nodoB<clave>* d){
		d_ = d;
	}

};